/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#define PRESCALER_VALUE ((uint16_t)8400-1)  // Prescaler for TIM6 to generate a 1-second interrupt
#define TIMER_PERIOD ((uint16_t)10000-1)    // Period for TIM6 to generate a 1-second interrupt
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim6;

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM6_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

// Records to configure the sensor
typedef enum bmp280_register_address_{
	BMP280_REG_ADDR_ERROR           = 0x00,
	BMP280_REG_ADDR_CAL_START_ADDR  = 0x88,
	BMP280_REG_ADDR_ID              = 0xD0,
	BMP280_REG_ADDR_RESET           = 0xE0,
	BMP280_REG_ADDR_STATUS          = 0xF3,
	BMP280_REG_ADDR_CTRL_MEAS       = 0xF4,
	BMP280_REG_ADDR_CONFIG          = 0xF5,
	BMP280_REG_ADDR_PRESS_MSB       = 0xF7,
	BMP280_REG_ADDR_PRESS_LSB       = 0xF8,
	BMP280_REG_ADDR_PRESS_XLSB      = 0xF9,
	BMP280_REG_ADDR_TEMP_MSB        = 0xFA,
	BMP280_REG_ADDR_TEMP_LSB        = 0xFB,
	BMP280_REG_ADDR_TEMP_XLSB       = 0xFC,

} bmp280_register_address_t;

// Routine for writing
HAL_StatusTypeDef bmp280_write_register(uint8_t sensor_register, uint8_t value){
	HAL_StatusTypeDef ret_val;

	uint8_t data_to_write[2] = {sensor_register, value};

	HAL_GPIO_WritePin(CS_BMP280_GPIO_Port, CS_BMP280_Pin, GPIO_PIN_RESET);
	ret_val = HAL_SPI_Transmit(&hspi1, data_to_write, 2, 100);
	HAL_GPIO_WritePin(CS_BMP280_GPIO_Port, CS_BMP280_Pin, GPIO_PIN_SET);

	return(ret_val);
}

// Routine for reading
HAL_StatusTypeDef bmp280_read_register(uint8_t sensor_register, uint8_t *value){
	HAL_StatusTypeDef ret_val = HAL_ERROR;
	uint8_t bmp280_reg = sensor_register;
	HAL_GPIO_WritePin(CS_BMP280_GPIO_Port, CS_BMP280_Pin, GPIO_PIN_RESET);
	if(HAL_SPI_Transmit(&hspi1, &bmp280_reg, 1, 100) == HAL_OK){
		ret_val = HAL_SPI_Receive(&hspi1, value, 1, 100);
	}
	HAL_GPIO_WritePin(CS_BMP280_GPIO_Port, CS_BMP280_Pin, GPIO_PIN_SET);
	return(ret_val);
}

// Function bleed/Read TEMP
int32_t bmp280_get_temperature(uint8_t *temp_registers){
	return ((int32_t) (temp_registers[0] << 12) | (temp_registers[1] << 4) | (temp_registers[2] >> 4));
}

// Function bleed/Read PRESS
int32_t bmp280_get_pressure(uint8_t *press_registers){
	return (int32_t) (press_registers[0] << 12) | (press_registers[1] << 4) | (press_registers[2] >> 4);
}
// Function to convert to degrees
float bmp280_convert_temperature_to_degrees(int32_t raw_temp){
	float var1 = 0;
	float var2 = 0;

	var1 = (float)((raw_temp) / 16384.0 - (27504) / 1024.0) * (26435);
	var2 = (float)(raw_temp / 131072.0 - 27504/8192.0);
	var2 = var2 * var2;
	var2 = var2 * (-1000);

	return ((float)(var1 + var2) / 5120.0);

}

// Function to convert to pascal
float bmp280_convert_pressure_to_pascal(int32_t raw_temp, int32_t raw_press){

	float p = 0;
	float t_fine = 0;
	float var1 = 0;
	float var2 = 0;

	// Calculate t_fine
	var1 = (float)((raw_temp) / 16384.0 - (27504) / 1024.0) * (26435);
	var2 = (float)(raw_temp / 131072.0 - 27504/8192.0);
	var2 = var2 * var2;
	var2 = var2 * (-1000);
	t_fine = (var1 + var2);

	// Calculate pressure in pascal
    var1 = (t_fine / 2.0) - 64000.0;
    var2 = var1 * var1 * (-7.0) / 32768.0;
    var2 = var2 + (var1 * (140.0) * 2.0);
    var2 = (var2 / 4.0) + (2855.0 * 65536.0);
    var1 = (((3024.0) * var1 * var1 / 524288.0) + ((-10685.0) * var1)) / 524288.0;
    var1 = (1.0 + var1 / 32768.0) * 36477.0;
    p = 1048576.0 - (float)raw_press;
    p = (p - (var2 / 4096.0)) * 6250.0 / var1;
    var1 = 6000.0 * p * p / 2147483648.0;
    var2 = p * (-14600.0) / 32768.0;

    return (p + (var1 + var2 + ((float)15500.0)) / 16.0);

}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_TIM6_Init();
  /* USER CODE BEGIN 2 */

  // Timer variable
  int16_t timer_val;

  // Measurement variables
  uint8_t read_data[3];
  uint8_t read_data1[3];
  int32_t raw_temperature;
  int32_t raw_pressure;
  float temperature_in_celcius = 0;
  float pressure_in_pascal = 0;

  //BMP280_REG_ADDR_CTRL_MEAS
  bmp280_read_register(BMP280_REG_ADDR_ID, read_data);
  HAL_Delay(100);

  // Set sensor to read continuously mode normal
  //BMP280_REG_ADDR_CTRL_MEAS / BMP280_REF_ADDR_CONFIG
  bmp280_write_register((0b01111111 & BMP280_REG_ADDR_CTRL_MEAS), 0b10110111);  // Maximum resolution and Mode normal
  bmp280_write_register((0b01111111 & BMP280_REG_ADDR_CONFIG), 0b10000000);     // 500ms tstandby

  // Start Timer and Get current timer
  HAL_TIM_Base_Start(&htim6);
  timer_val = __HAL_TIM_GET_COUNTER(&htim6);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  // If 1 second passes, read the sensor
	  if(__HAL_TIM_GET_COUNTER(&htim6) - timer_val >= 10000){
		  // Read TEMP and PRESS
		  bmp280_read_register(BMP280_REG_ADDR_TEMP_MSB, read_data);
		  bmp280_read_register(BMP280_REG_ADDR_PRESS_MSB, read_data1);

		  // Read TEMP and PRESS
		  raw_temperature= bmp280_get_temperature(read_data);
		  raw_pressure= bmp280_get_pressure(read_data1);

		  // Read TEMP and PRESS
		  temperature_in_celcius = bmp280_convert_temperature_to_degrees(raw_temperature);
		  pressure_in_pascal= bmp280_convert_pressure_to_pascal(raw_temperature, raw_pressure);

		  timer_val = __HAL_TIM_GET_COUNTER(&htim6);  // Repeat to do it every second


	  }

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 64;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 8000-1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 65535;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CS_BMP280_GPIO_Port, CS_BMP280_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : CS_BMP280_Pin */
  GPIO_InitStruct.Pin = CS_BMP280_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CS_BMP280_GPIO_Port, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
